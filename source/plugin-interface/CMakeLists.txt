set(NAME interface)

add_executable(${NAME} PluginInterface.c )

#if(BUILD_LINUX)
    target_compile_definitions(${NAME} PRIVATE BUILD_LINUX)
#endif()
#
#if(BUILD_WINDOWS)
#    target_compile_definitions(${NAME} PRIVATE BUILD_WINDOWS)
#endif()