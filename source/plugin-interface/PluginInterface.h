#pragma once

int PluginInterface(const char* pluginPath, const char* dataPath,
                    const char* dataFormatPath, const char* resultFilePath);