#ifdef BUILD_WINDOWS
//todo прописать линковку интерфейса dll для windows. Вынести общие моменты
#endif


#ifdef BUILD_LINUX
#include <stdio.h>
#include <dlfcn.h>
#include "PluginInterface.h"

int main() {
    //тестовые пути
    const char* pluginPath = "../shared/libplugin.so";
    const char* dataPath = "testname";
    const char* dataFormatPath = "";
    const char* resultFilePath = "./result/testname111result";
    return PluginInterface(pluginPath, dataPath, dataFormatPath, resultFilePath);

    return 0;
}

int PluginInterface(const char* pluginPath, const char* dataPath,
                    const char* dataFormatPath, const char* resultFilePath)
{
    void *lib_handle = NULL;
    int (*PluginExec) (const char*, const char*, const char*);

    int result = 0;
    lib_handle = dlopen(pluginPath, RTLD_LAZY);
    if(lib_handle)
    {
        printf("dlopen returns %p\n", lib_handle);
        PluginExec = dlsym(lib_handle, "PluginExec");
        if(PluginExec)
        {
            printf("dlsym returns add = %p\n", PluginExec);
            result = PluginExec(dataPath, dataFormatPath, resultFilePath);
        }
        else
        {
            printf("Function entry not found in DLL: %s", dlerror());
        }
        dlclose(lib_handle);
    }
    else
    {
        printf("Unable to open DLL");
    }
    return result;
}
#endif