set(NAME plugin)

file(GLOB_RECURSE PLUGIN_SOURCES RELATIVE_PATH
        ${CMAKE_SOURCE_DIR}/source/*.h
        ${CMAKE_SOURCE_DIR}/source/*.cpp
        )

add_library(${NAME} SHARED ${PLUGIN_SOURCES})
target_include_directories(${NAME} PRIVATE ${CMAKE_SOURCE_DIR}/source/)