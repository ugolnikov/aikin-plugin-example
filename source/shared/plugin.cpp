#include <iostream>
#include <fstream>
#include <vector>
#include <filesystem>
#include "plugin.h"

//Здесь можно определить содержимое модуля

enum Status {
    RESULT_SUCCESSFULL_CREATED,
    FAIL
};

/* Структура файла данных (позже будет описываться в отдельном файле)
 *
 * количество кейсов                (целое число)
 *      количество данных в кейсе   (целое число)
 *          данные                  (целое число)
 * */

int Summ(std::vector<int>& values)
{
    int result = 0;
    for(auto& val: values)
    {
        result += val;
    }
    return result;
}

void Execute(std::ofstream& resultFile, std::ifstream& dataFile)
{
    int setCount = 0;
    dataFile >> setCount;

    for(int index = 0; index < setCount; index++)
    {
        int setSize = 0;
        dataFile >> setSize;

        std::vector<int> set(setSize);
        for (auto& val: set)
        {
            dataFile >> val;
        }

        resultFile << Summ(set) << std::endl;
    }
}

int PluginExec(const char* dataPath, const char* dataFormatPath, const char* resultFilePath)
{
    Status status {FAIL};

    if (std::filesystem::exists(dataPath)) {
        std::cout << "Plugin: data file exist: " << std::endl;

        std::filesystem::permissions(
                dataPath,
                std::filesystem::perms::owner_read | std::filesystem::perms::group_read | std::filesystem::perms::others_read,
                std::filesystem::perm_options::add
        );
    }

    std::ifstream dataFile;
    dataFile.open(dataPath);
    if (!dataFile.is_open()) {
        std::cout << "Plugin: Can not open data file: " << std::endl;
    }

    std::ofstream resultFile;
    resultFile.open(resultFilePath);
    if (!resultFile.is_open()) {
        std::cout << "Plugin: Can not open result file: " << std::endl;
    }

    if (std::filesystem::exists(resultFilePath)) {
        std::cout << "Plugin: result file exist: " << std::endl;

        std::filesystem::permissions(
                resultFilePath,
                std::filesystem::perms::group_all | std::filesystem::perms::others_all,
                std::filesystem::perm_options::add
        );
    }

    Execute(resultFile, dataFile);

    std::cout << "Plugin: Ends with status: " << status << std::endl;
    return status;
}